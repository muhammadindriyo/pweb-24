addEventListener("DOMContentLoaded", () => {
	const waktu = new Date();
	const isiwaktu = waktu.toTimeString().substring(0, 5);
	document.getElementById("waktu").value = isiwaktu;

	const tanggal = new Date();
	const hari = String(tanggal.getDate()).padStart(2, "0");
	const bulan = String(tanggal.getMonth() + 1).padStart(2, "0");
	const tahun = tanggal.getFullYear();

	const isitanggal = `${tahun}-${bulan}-${hari}`;
	document.getElementById("tanggal").value = isitanggal;

	document.getElementById("form-a").addEventListener("submit", (e) => {
		// e.preventDefault();
		const username = document.getElementById("name").value;
		const komentar = document.getElementById("komentar").value;
		const email = document.getElementById("email").value;
		const emailregex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

		const datacheck = () => {
			if (username === "" && email === "") {
				alert("Nama dan Email tidak boleh kosong");
				return false;
			}
			if (username === "") {
				alert("Nama tidak boleh kosong");
				return false;
			}
			if (email === "") {
				alert("Email tidak boleh kosong");
				return false;
			}
			if (komentar === "") {
				alert("Komentar tidak boleh kosong");
				return false;
			}
			if (username.length <= 3) {
				alert("Nama tidak boleh kurang dari 3 karakter");
				return false;
			}
			if (!emailregex.test(email)) {
				alert("Email tidak valid");
				return false;
			}
			alert("Berhasil Mengirim Komentar");
		};
		datacheck(username, komentar, email);
	});
});

