const judul = document.title;
window.addEventListener("blur", () => {
	document.title = "Jangan Pergi :(";
});
window.addEventListener("focus", () => {
	document.title = judul;
});
function kontak() {
	const artikel = document.getElementById("artikel-utama");
        artikel.innerHTML = `
        <div class="kontak-saya">
            <h3>Kontak Saya</h3>
            <div class="isi-kontak">
                <img src="https://upload.wikimedia.org/wikipedia/commons/5/51/Facebook_f_logo_%282019%29.svg" alt=""><a href="https://www.facebook.com/muhammadindriyo12">Facebook</a>
                <img src="https://upload.wikimedia.org/wikipedia/commons/9/95/Instagram_logo_2022.svg" alt=""><a href="https://www.instagram.com/ikankardus/">Instagram</a> 
            </div>
            <div class="form-kontak">
                <form method="POST">
                    <label for="name">Nama:</label>
                    <input type="text" id="name" name="name" placeholder="Nama"required>
                    <label for="email">Email:</label>
                    <input type="email" id="email" name="email" placeholder="Email" required>
                    <label for="subject">Subjek:</label>
                    <input type="text" id="subject" name="subject" placeholder="Subjek" required>
                    <label for="Pesan">Pesan:</label>
                    <textarea id="komentar" name="pesan" placeholder="Pesan" required></textarea>
                    <input type="submit" value="Kirim">
                </form>
                </form>
            </div>
        </div>
        `;
    
}
